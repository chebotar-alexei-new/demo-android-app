package ru.alex.demoone.dagger.module;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.requery.Persistable;
import io.requery.android.sqlite.DatabaseSource;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveSupport;
import io.requery.sql.Configuration;
import io.requery.sql.EntityDataStore;
import io.requery.sql.TableCreationMode;
import ru.alex.demoone.BuildConfig;
import ru.alex.demoone.data.database.models.Models;
/**
 * Предоставляет зависимость для БД.
 */
@Module
@Singleton
public class DatabaseModule {

    @Provides
    @Singleton
    ReactiveEntityStore<Persistable> persistableSingleEntityStore(Application application) {
        // override onUpgrade to handle migrating to a new version
        DatabaseSource source = new DatabaseSource(application, Models.DEFAULT, 1);
        if (BuildConfig.DEBUG) {
            // use this in development mode to drop and recreate the tables on every upgrade
            source.setTableCreationMode(TableCreationMode.DROP_CREATE);
        }
        Configuration configuration = source.getConfiguration();
        return ReactiveSupport.toReactiveStore(new EntityDataStore<Persistable>(configuration));
    }


}

