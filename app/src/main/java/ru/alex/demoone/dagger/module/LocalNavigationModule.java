package ru.alex.demoone.dagger.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.alex.demoone.presentation.bottom.LocalCiceroneHolder;
/**
 * Предоставляет зависимость для навигации внутри вкладки.
 */
@Module
public class LocalNavigationModule {

    @Provides
    @Singleton
    LocalCiceroneHolder provideLocalNavigationHolder() {
        return new LocalCiceroneHolder();
    }
}
