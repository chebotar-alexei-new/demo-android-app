package ru.alex.demoone.dagger;

import javax.inject.Singleton;

import dagger.Component;
import ru.alex.demoone.dagger.module.ApplicationModule;
import ru.alex.demoone.dagger.module.DatabaseModule;
import ru.alex.demoone.dagger.module.LocalNavigationModule;
import ru.alex.demoone.dagger.module.NavigationModule;
import ru.alex.demoone.dagger.module.NetworkModule;
import ru.alex.demoone.presentation.bottom.BottomNavigationActivity;
import ru.alex.demoone.presentation.bottom.BottomNavigationPresenter;
import ru.alex.demoone.presentation.bottom.TabContainerFragment;
import ru.alex.demoone.presentation.taba.main.MainPresenter;
import ru.alex.demoone.presentation.tabb.catalog.CatalogPresenter;
import ru.alex.demoone.presentation.tabc.market.MarketPresenter;
import ru.alex.demoone.presentation.tabe.profile.ProfilePresenter;
import ru.alex.demoone.presentation.tabe.login.LoginPresenter;
import ru.alex.demoone.presentation.tabd.card.CardPresenter;

/**
 * Добавляет обьекты в граф зависимостей.
 */
@Singleton
@Component(modules = {
        NavigationModule.class,
        NetworkModule.class,
        DatabaseModule.class,
        ApplicationModule.class,
        LocalNavigationModule.class
})
public interface AppComponent {

    void inject(BottomNavigationActivity activity);

    void inject(TabContainerFragment fragment);

    void inject(BottomNavigationPresenter presenter);

    void inject(LoginPresenter presenter);

    void inject(CardPresenter presenter);

    void inject(ProfilePresenter presenter);

    void inject(MarketPresenter presenter);

    void inject(CatalogPresenter presenter);

    void inject(MainPresenter presenter);
}
