package ru.alex.demoone;

import android.app.Application;

import ru.alex.demoone.dagger.AppComponent;
import ru.alex.demoone.dagger.DaggerAppComponent;
import ru.alex.demoone.dagger.module.ApplicationModule;

public class App extends Application {

    public static App INSTANCE;
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        appComponent = DaggerAppComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
