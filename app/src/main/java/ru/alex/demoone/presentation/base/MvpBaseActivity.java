package ru.alex.demoone.presentation.base;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import ru.alex.demoone.R;
import ru.alex.demoone.data.network.RetrofitException;

public abstract class MvpBaseActivity extends MvpAppCompatActivity implements MvpBaseView {

    private Toast toast;
    protected CompositeDisposable compositeDisposable = new CompositeDisposable();
    protected View rootView;

    @Nullable
    @BindView(R.id.progressBar)
    View progressBar;

    protected void addSub(Disposable subscription) {
        compositeDisposable.add(subscription);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null && !compositeDisposable.isDisposed())
            compositeDisposable.dispose();
    }

    protected abstract int setLayoutRes();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setLayoutRes());
        rootView = findViewById(android.R.id.content);
        ButterKnife.bind(this, rootView);
        setupUI(rootView);
    }

    public void setupUI(View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener((v, event) -> {
                hideSoftKeyboard(MvpBaseActivity.this);
                return false;
            });
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    protected void hideSoftKeyboard(Activity activity) {
        if (activity == null) return;
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showSoftKeyboard(Activity activity) {
        if (activity == null) return;
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, 0);
        }
    }

    protected void showToast(String message) {
        if (toast != null) toast.cancel();
        toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        toast.show();
    }

    protected void showToast(int message) {
        if (toast != null) toast.cancel();
        toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        toast.show();
    }

    protected void showDialog(Context context, String title, String message) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, null)
                .create()
                .show();
    }

    protected void showDialog(Context context, String title, String message,DialogInterface.OnClickListener lis) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, lis)
                .create()
                .show();
    }

    @Override
    public void showConnectionError() {
        showToast(getString(R.string.error_connection));
    }

    @Override
    public void showServerError(Throwable throwable) {
        throwable.printStackTrace();
        if (throwable instanceof RetrofitException && ((RetrofitException) throwable).errorMessageToUser != null)
            showToast(((RetrofitException) throwable).errorMessageToUser);
        else if (throwable instanceof RetrofitException && ((RetrofitException) throwable).getKind() != RetrofitException.Kind.NETWORK)
            showConnectionError();
        else
            showToast(getString(R.string.error_server_failed));
    }

    @Override
    public void showInternalError() {
        showToast(getString(R.string.error_internal));
    }

    @Override
    public void showErrorMessage(String s) {
        showToast(s);
    }

    @Override
    public void showErrorMessage(int s) {
        showToast(s);
    }

    public void showLoading(boolean b) {
        if (progressBar != null)
            progressBar.setVisibility(b ? View.VISIBLE : View.GONE);
    }
}
