package ru.alex.demoone.presentation.base.adapter;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;

public class RecyclerViewItemDiffCallback<T extends RecyclerViewItem> extends DiffUtil.ItemCallback<T> {

    @Override
    public boolean areItemsTheSame(@NonNull final T oldItem, @NonNull final T newItem) {
        return oldItem.getClass().equals(newItem.getClass()) && oldItem.getLongId() == newItem.getLongId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull final T oldItem, @NonNull final T newItem) {
        return oldItem.equals(newItem);
    }
}