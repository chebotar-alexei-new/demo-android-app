package ru.alex.demoone.presentation.tabe.profile;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import ru.alex.demoone.domain.interactors.UserInteractor;
import ru.alex.demoone.presentation.base.Screens;
import ru.alex.demoone.presentation.tabe.login.LoginFragment;
import ru.terrakok.cicerone.Router;
import ru.alex.demoone.App;
import ru.alex.demoone.presentation.base.MvpBasePresenter;


@InjectViewState
public class ProfilePresenter extends MvpBasePresenter<ProfileView> {

    private Router router;
    @Inject
    UserInteractor userInteractor;

    ProfilePresenter(Router router) {
        App.INSTANCE.getAppComponent().inject(this);
        this.router = router;
    }

    @Override
    public void attachView(ProfileView view) {
        super.attachView(view);
        getViewState().setInitData("Profile");
    }

    public void onBackPressed() {
        router.exit();
    }

    public void onLogoutPressed() {
        userInteractor.logout();
        router.newRootScreen(Screens.LOGIN_SCREEN, LoginFragment.getBundle());
    }
}
