package ru.alex.demoone.presentation.taba.main;

import com.arellomobile.mvp.InjectViewState;

import ru.terrakok.cicerone.Router;
import ru.alex.demoone.App;
import ru.alex.demoone.presentation.base.MvpBasePresenter;

@InjectViewState
public class MainPresenter extends MvpBasePresenter<MainView> {
    private Router router;

    MainPresenter(Router router) {
        App.INSTANCE.getAppComponent().inject(this);
        this.router = router;
    }

    @Override
    public void attachView(MainView view) {
        super.attachView(view);
        getViewState().setInitData("Main");
    }

    public void onBackPressed() {
        router.exit();
    }
}
