package ru.alex.demoone.presentation.tabc.market;

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import ru.alex.demoone.presentation.base.MvpBaseView;


@StateStrategyType(OneExecutionStateStrategy.class)
public interface MarketView extends MvpBaseView {

    void setInitData(String data);
}
