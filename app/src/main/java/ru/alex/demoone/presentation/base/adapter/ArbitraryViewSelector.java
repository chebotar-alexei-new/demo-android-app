package ru.alex.demoone.presentation.base.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public final class ArbitraryViewSelector {

    @NonNull
    private final List<Cell> cellList = new ArrayList<>();

    public void addCell(@NonNull final Cell cell) {
        cellList.add(cell);
    }

    public void removeCell(@NonNull final Cell cell) {
        cellList.remove(cell);
    }

    @NonNull
    public Cell getCell(@NonNull final RecyclerViewItem item) {
        for (final Cell cell : cellList) {
            if (cell.isItemForThisCell(item)) {
                return cell;
            }
        }
        throw new NoSuchRecyclerRowException(item);
    }

    @NonNull
    public Cell getCell(final int viewType) {
        for (final Cell cell : cellList) {
            if (cell.type() == viewType) {
                return cell;
            }
        }
        throw new NoSuchRecyclerRowException(viewType);
    }

    public interface Cell {

        boolean isItemForThisCell(@NonNull final RecyclerViewItem item);

        int type();

        @NonNull
        RecyclerView.ViewHolder holder(@NonNull final ViewGroup parent);

        void bind(@NonNull final RecyclerView.ViewHolder holder, @NonNull final BindableConfiguration<RecyclerViewItem> binder);
    }
}
