package ru.alex.demoone.presentation.tabe.login;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;
import ru.alex.demoone.App;
import ru.alex.demoone.domain.interactors.LoginScreenInteractor;
import ru.alex.demoone.domain.interactors.UserInteractor;
import ru.alex.demoone.presentation.base.MvpBasePresenter;
import ru.alex.demoone.presentation.base.Screens;
import ru.alex.demoone.presentation.tabe.profile.ProfileFragment;

@InjectViewState
public class LoginPresenter extends MvpBasePresenter<LoginView> {

    @Inject
    LoginScreenInteractor loginScreenInteractor;

    @Inject
    UserInteractor userInteractor;
    private final Router router;

    LoginPresenter(Router router) {
        App.INSTANCE.getAppComponent().inject(this);
        this.router = router;
    }

    public void onBackPressed() {
        router.exit();
    }

    void onAuthPressed(String email, String pass) {
        addSub(loginScreenInteractor
                .auth(email, pass)
                .doOnSubscribe(subscription -> getViewState().showLoading(true))
                .doOnTerminate(() -> getViewState().showLoading(false))
                .subscribe(
                        result -> {
                            userInteractor.saveUser(result.data);
                            router.newRootScreen(Screens.PROFILE_SCREEN, ProfileFragment.getBundle());
                        },
                        throwable -> getViewState().showServerError(throwable)));
    }

    void onForgotPassPressed(String email) {
        getViewState().showErrorMessage("Восстановление пароля");
    }
}
