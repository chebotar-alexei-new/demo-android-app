package ru.alex.demoone.presentation.bottom;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import java.text.NumberFormat;

import javax.inject.Inject;

import butterknife.BindView;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.commands.Back;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Replace;
import ru.terrakok.cicerone.commands.SystemMessage;
import ru.alex.demoone.App;
import ru.alex.demoone.R;
import ru.alex.demoone.presentation.base.BackButtonListener;
import ru.alex.demoone.presentation.base.MvpBaseActivity;
import ru.alex.demoone.presentation.base.RouterProvider;
import ru.alex.demoone.presentation.base.Screens;
import ru.alex.demoone.presentation.tabd.card.CardFragment;
import ru.alex.demoone.presentation.tabe.login.LoginFragment;
import ru.alex.demoone.presentation.tabe.profile.ProfileFragment;
import ru.alex.demoone.utils.BottomNavigationViewHelper;

public class BottomNavigationActivity extends MvpBaseActivity implements IBottomNavigationView, RouterProvider {

    private TabContainerFragment mainTabFragment;
    private TabContainerFragment catalogTabFragment;
    private TabContainerFragment marketsTabFragment;
    private TabContainerFragment cardTabFragment;
    private TabContainerFragment profileTabFragment;

    @Inject
    Router router;

    @Inject
    NavigatorHolder navigatorHolder;

    @InjectPresenter
    public BottomNavigationPresenter presenter;

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;

    TextView cartCount;

    @ProvidePresenter
    public BottomNavigationPresenter createBottomNavigationPresenter() {
        return new BottomNavigationPresenter(router);
    }

    public static void start(Context context) {
        context.startActivity(intent(context));
    }

    @NonNull
    private static Intent intent(Context context) {
        return new Intent(context, BottomNavigationActivity.class);
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.activity_bottom;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.INSTANCE.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);

        initViews();
        initContainers();

        if (savedInstanceState == null) {
            bottomNavigationView.setSelectedItemId(MAIN_TAB_POSITION);
            presenter.onTabMainClick();
        }
    }

    private void initViews() {
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_main:
                    presenter.onTabMainClick();
                    break;
                case R.id.action_catalog:
                    presenter.onTabCatalogClick();
                    break;
                case R.id.action_markets:
                    presenter.onTabMarketsClick();
                    break;
                case R.id.action_card:
                    presenter.onTabCardClick();
                    break;
                case R.id.action_profile:
                    presenter.onTabProfileClick();
                    break;
            }
            return true;
        });
        FrameLayout cartTab = (FrameLayout) ((BottomNavigationMenuView) bottomNavigationView.getChildAt(0)).getChildAt(IBottomNavigationView.CARD_TAB_POSITION);
        getLayoutInflater().inflate(R.layout.cart_count_bottom_view, cartTab, true);
        cartCount = cartTab.findViewById(R.id.cart_count);
    }

    private void initContainers() {
        FragmentManager fm = getSupportFragmentManager();
        mainTabFragment = (TabContainerFragment) fm.findFragmentByTag("MAIN");
        if (mainTabFragment == null) {
            mainTabFragment = TabContainerFragment.getNewInstance(Screens.MAIN_SCREEN);
            fm.beginTransaction()
                    .add(R.id.container, mainTabFragment, "MAIN")
                    .detach(mainTabFragment).commitNow();

        }

        catalogTabFragment = (TabContainerFragment) fm.findFragmentByTag("CATALOG");
        if (catalogTabFragment == null) {
            catalogTabFragment = TabContainerFragment.getNewInstance(Screens.CATALOG_SCREEN);
            fm.beginTransaction()
                    .add(R.id.container, catalogTabFragment, "CATALOG")
                    .detach(catalogTabFragment).commitNow();
        }

        marketsTabFragment = (TabContainerFragment) fm.findFragmentByTag("MARKETS");
        if (marketsTabFragment == null) {
            marketsTabFragment = TabContainerFragment.getNewInstance(Screens.MARKETS_SCREEN);
            fm.beginTransaction()
                    .add(R.id.container, marketsTabFragment, "MARKETS")
                    .detach(marketsTabFragment).commitNow();
        }

        cardTabFragment = (TabContainerFragment) fm.findFragmentByTag("CARD");
        if (cardTabFragment == null) {
            cardTabFragment = TabContainerFragment.getNewInstance(Screens.CARD_SCREEN, CardFragment.getBundle());
            fm.beginTransaction()
                    .add(R.id.container, cardTabFragment, "CARD")
                    .detach(cardTabFragment).commitNow();
        }

        profileTabFragment = (TabContainerFragment) fm.findFragmentByTag("PROFILE");
        if (profileTabFragment == null) {
            profileTabFragment = presenter.isUserLogined()
                    ? TabContainerFragment.getNewInstance(Screens.PROFILE_SCREEN, ProfileFragment.getBundle())
                    : TabContainerFragment.getNewInstance(Screens.LOGIN_SCREEN, LoginFragment.getBundle());
            fm.beginTransaction()
                    .add(R.id.container, profileTabFragment, "PROFILE")
                    .detach(profileTabFragment).commitNow();
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment != null
                && fragment instanceof BackButtonListener
                && ((BackButtonListener) fragment).onBackPressed()) {
            return;
        } else {
            presenter.onBackPressed();
        }
    }

    private Navigator navigator = new Navigator() {
        @Override
        public void applyCommand(Command command) {
            if (command instanceof Back) {
                presenter.onBackPressed();
            } else if (command instanceof SystemMessage) {
                Toast.makeText(BottomNavigationActivity.this, ((SystemMessage) command).getMessage(), Toast.LENGTH_SHORT).show();
            } else if (command instanceof Replace) {
                FragmentManager fm = getSupportFragmentManager();
                switch (((Replace) command).getScreenKey()) {
                    case Screens.TAB_MAIN_SCREEN:
                        if (mainTabFragment.isDetached()) {
                            mainTabFragment.setShouldNotAnimate(true);
                            catalogTabFragment.setShouldNotAnimate(true);
                            marketsTabFragment.setShouldNotAnimate(true);
                            cardTabFragment.setShouldNotAnimate(true);
                            profileTabFragment.setShouldNotAnimate(true);
                            fm.beginTransaction()
                                    .setCustomAnimations(
                                            R.anim.fade_in,
                                            R.anim.fade_out,
                                            R.anim.fade_in,
                                            R.anim.fade_out)
                                    .detach(catalogTabFragment)
                                    .detach(marketsTabFragment)
                                    .detach(cardTabFragment)
                                    .detach(profileTabFragment)
                                    .attach(mainTabFragment)
                                    .commit();
                        } else {
                            mainTabFragment.getRouter().backTo(Screens.MAIN_SCREEN);
                        }
                        break;
                    case Screens.TAB_CATALOG_SCREEN:
                        if (catalogTabFragment.isDetached()) {
                            mainTabFragment.setShouldNotAnimate(true);
                            catalogTabFragment.setShouldNotAnimate(true);
                            marketsTabFragment.setShouldNotAnimate(true);
                            cardTabFragment.setShouldNotAnimate(true);
                            profileTabFragment.setShouldNotAnimate(true);
                            fm.beginTransaction()
                                    .setCustomAnimations(
                                            R.anim.fade_in,
                                            R.anim.fade_out,
                                            R.anim.fade_in,
                                            R.anim.fade_out)
                                    .detach(mainTabFragment)
                                    .detach(marketsTabFragment)
                                    .detach(cardTabFragment)
                                    .detach(profileTabFragment)
                                    .attach(catalogTabFragment)
                                    .commit();
                        } else {
                            catalogTabFragment.getRouter().backTo(Screens.CATALOG_SCREEN);
                        }

                        break;
                    case Screens.TAB_MARKETS_SCREEN:
                        if (marketsTabFragment.isDetached()) {
                            mainTabFragment.setShouldNotAnimate(true);
                            catalogTabFragment.setShouldNotAnimate(true);
                            marketsTabFragment.setShouldNotAnimate(true);
                            cardTabFragment.setShouldNotAnimate(true);
                            profileTabFragment.setShouldNotAnimate(true);
                            fm.beginTransaction()
                                    .setCustomAnimations(
                                            R.anim.fade_in,
                                            R.anim.fade_out,
                                            R.anim.fade_in,
                                            R.anim.fade_out)
                                    .detach(mainTabFragment)
                                    .detach(catalogTabFragment)
                                    .detach(cardTabFragment)
                                    .detach(profileTabFragment)
                                    .attach(marketsTabFragment)
                                    .commit();
                        } else {
                            marketsTabFragment.getRouter().backTo(Screens.MARKETS_SCREEN);
                        }
                        break;
                    case Screens.TAB_CARD_SCREEN:
                        if (cardTabFragment.isDetached()) {
                            mainTabFragment.setShouldNotAnimate(true);
                            catalogTabFragment.setShouldNotAnimate(true);
                            marketsTabFragment.setShouldNotAnimate(true);
                            cardTabFragment.setShouldNotAnimate(true);
                            profileTabFragment.setShouldNotAnimate(true);
                            fm.beginTransaction()
                                    .setCustomAnimations(
                                            R.anim.fade_in,
                                            R.anim.fade_out,
                                            R.anim.fade_in,
                                            R.anim.fade_out)
                                    .detach(mainTabFragment)
                                    .detach(catalogTabFragment)
                                    .detach(marketsTabFragment)
                                    .detach(profileTabFragment)
                                    .attach(cardTabFragment)
                                    .commit();
                        } else {
                            cardTabFragment.getRouter().backTo(Screens.CARD_SCREEN);
                        }
                        break;
                    case Screens.TAB_PROFILE_SCREEN:
                        if (profileTabFragment.isDetached()) {
                            mainTabFragment.setShouldNotAnimate(true);
                            catalogTabFragment.setShouldNotAnimate(true);
                            marketsTabFragment.setShouldNotAnimate(true);
                            cardTabFragment.setShouldNotAnimate(true);
                            profileTabFragment.setShouldNotAnimate(true);
                            fm.beginTransaction()
                                    .setCustomAnimations(
                                            R.anim.fade_in,
                                            R.anim.fade_out,
                                            R.anim.fade_in,
                                            R.anim.fade_out)
                                    .detach(mainTabFragment)
                                    .detach(catalogTabFragment)
                                    .detach(marketsTabFragment)
                                    .detach(cardTabFragment)
                                    .attach(profileTabFragment)
                                    .commit();
                        } else {
                            profileTabFragment.getRouter().backTo(Screens.PROFILE_SCREEN);
                        }
                        break;
                }
            }
        }
    };

    public void selectTab(int itemId) {
        bottomNavigationView.setSelectedItemId(itemId);
    }

    public void refreshMainRoute() {
        mainTabFragment.getRouter().backTo(Screens.MAIN_SCREEN);
    }

    public void refreshCatalogRoute() {
        catalogTabFragment.getRouter().backTo(Screens.CATALOG_SCREEN);
    }

    @Override
    public Router getRouter() {
        return router;
    }

    public void showTabbar(boolean b) {
        bottomNavigationView.setVisibility(b ? View.VISIBLE : View.GONE);
    }

    private void setBottomCartText(int count) {
        if (count > 0) {
            cartCount.setText(NumberFormat.getInstance().format(count));
            cartCount.setVisibility(View.VISIBLE);
        } else {
            cartCount.setVisibility(View.GONE);
        }
    }

    public static void restart(@NonNull Context context) {
        context.startActivity(new Intent(context, BottomNavigationActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    @Override
    public void exit() {
        finish();
    }

//    @Override
//    public void navigateToNews(String id) {
//        FragmentManager fm = getSupportFragmentManager();
//        mainTabFragment = (TabContainerFragment) fm.findFragmentByTag("MAIN");
//        if (mainTabFragment == null) {
//            mainTabFragment = TabContainerFragment.getNewInstance(Screens.NEWS_ITEM_SCREEN, NewsItemFragment.getArguments(id));
//            fm.beginTransaction()
//                    .replace(R.id.container, mainTabFragment, "MAIN")
//                    .commitNow();
//
//        }
//    }
}
