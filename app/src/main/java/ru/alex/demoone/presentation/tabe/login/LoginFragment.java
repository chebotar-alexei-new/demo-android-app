package ru.alex.demoone.presentation.tabe.login;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import butterknife.BindView;
import ru.alex.demoone.R;
import ru.alex.demoone.presentation.base.BackButtonListener;
import ru.alex.demoone.presentation.base.MvpBaseFragment;
import ru.alex.demoone.presentation.base.RouterProvider;

public class LoginFragment extends MvpBaseFragment implements BackButtonListener, LoginView {

    @BindView(R.id.enter)
    TextView enter;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.forgotPass)
    TextView forgotPass;

    @InjectPresenter
    LoginPresenter presenter;


    @ProvidePresenter
    LoginPresenter provideTutorialPresenter() {
        return new LoginPresenter(((RouterProvider) getParentFragment()).getRouter());
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_login;
    }


    public static Fragment getNewInstance(Bundle data) {
        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(data);
        return fragment;
    }

    public static Bundle getBundle() {
        Bundle arguments = new Bundle();
        return arguments;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews();
    }

    private void initViews() {
        enter.setOnClickListener(view -> presenter.onAuthPressed("email", password.getText().toString().trim()));
        forgotPass.setOnClickListener(view -> presenter.onForgotPassPressed(email.getText().toString().trim()));
    }

    @Override
    protected void configureToolbar(@NonNull Toolbar toolbar) {
        super.configureToolbar(toolbar);
        toolbar.setTitle(R.string.enter);
    }

    @Override
    public boolean onBackPressed() {
        presenter.onBackPressed();
        return true;
    }
}
