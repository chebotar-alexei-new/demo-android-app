package ru.alex.demoone.presentation.base.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class ArbitraryViewHolder<T> extends RecyclerView.ViewHolder {

    public ArbitraryViewHolder(@NonNull final View itemView) {
        super(itemView);
    }

    public abstract void bind(@NonNull final T item, @NonNull final BindableConfiguration<RecyclerViewItem> binder);
}