package ru.alex.demoone.presentation.base;

public interface BackButtonListener {
    boolean onBackPressed();
}
