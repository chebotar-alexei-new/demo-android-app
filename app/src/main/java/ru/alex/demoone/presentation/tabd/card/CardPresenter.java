package ru.alex.demoone.presentation.tabd.card;

import com.arellomobile.mvp.InjectViewState;

import ru.terrakok.cicerone.Router;
import ru.alex.demoone.App;
import ru.alex.demoone.presentation.base.MvpBasePresenter;


@InjectViewState
public class CardPresenter extends MvpBasePresenter<CardView> {
    private Router router;

    CardPresenter(Router router) {
        App.INSTANCE.getAppComponent().inject(this);
        this.router = router;
    }

    @Override
    public void attachView(CardView view) {
        super.attachView(view);
        getViewState().setInitData("Card");
    }

    public void onBackPressed() {
        router.exit();
    }
}
