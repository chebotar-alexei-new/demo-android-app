package ru.alex.demoone.presentation.tabe.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import javax.inject.Inject;

import butterknife.BindView;
import ru.alex.demoone.R;
import ru.alex.demoone.presentation.base.BackButtonListener;
import ru.alex.demoone.presentation.base.MvpBaseFragment;
import ru.alex.demoone.presentation.base.RouterProvider;

public class ProfileFragment extends MvpBaseFragment implements ProfileView, BackButtonListener {

    private static final String TAG = ProfileFragment.class.getSimpleName();

    @BindView(R.id.anyText)
    TextView anyTextView;
    @BindView(R.id.logout)
    TextView logoutView;
    @InjectPresenter
    ProfilePresenter presenter;

    @ProvidePresenter
    ProfilePresenter provideForwardPresenter() {
        return new ProfilePresenter(((RouterProvider) getParentFragment()).getRouter());
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_profile;
    }

    public static ProfileFragment getNewInstance(Bundle data) {
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(data);
        return fragment;
    }

    public static Bundle getBundle() {
        Bundle arguments = new Bundle();
        return arguments;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews();
    }

    private void initViews() {
        logoutView.setOnClickListener(v-> presenter.onLogoutPressed());
    }

    @Override
    public boolean onBackPressed() {
        presenter.onBackPressed();
        return true;
    }

    @Override
    public void setInitData(String data) {
        anyTextView.setText(data);
    }
}
