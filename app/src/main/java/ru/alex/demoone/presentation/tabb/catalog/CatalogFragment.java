package ru.alex.demoone.presentation.tabb.catalog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import butterknife.BindView;
import ru.alex.demoone.R;
import ru.alex.demoone.presentation.base.BackButtonListener;
import ru.alex.demoone.presentation.base.MvpBaseFragment;
import ru.alex.demoone.presentation.base.RouterProvider;

public class CatalogFragment extends MvpBaseFragment implements CatalogView, BackButtonListener {

    private static final String TAG = CatalogFragment.class.getSimpleName();

    @BindView(R.id.anyText)
    TextView anyTextView;
    @InjectPresenter
    CatalogPresenter presenter;

    @ProvidePresenter
    CatalogPresenter provideForwardPresenter() {
        return new CatalogPresenter(((RouterProvider) getParentFragment()).getRouter());
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_catalog;
    }

    public static CatalogFragment getNewInstance(Bundle data) {
        CatalogFragment fragment = new CatalogFragment();
        fragment.setArguments(data);
        return fragment;
    }

    public static Bundle getBundle() {
        Bundle arguments = new Bundle();
        return arguments;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews();
    }

    private void initViews() {

    }

    @Override
    protected void configureToolbar(@NonNull Toolbar toolbar) {
        super.configureToolbar(toolbar);
        toolbar.setTitle(R.string.menu_catalog);
    }

    @Override
    protected boolean showBackButton() {
        return true;
    }

    @Override
    public boolean onBackPressed() {
        presenter.onBackPressed();
        return true;
    }

    @Override
    public void setInitData(String data) {
        anyTextView.setText(data);
    }
}
