package ru.alex.demoone.presentation.bottom;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import ru.alex.demoone.presentation.base.MvpBaseView;


@StateStrategyType(AddToEndSingleStrategy.class)
public interface IBottomNavigationView extends MvpBaseView {
    int MAIN_TAB_POSITION = 0;
    int CATALOG_TAB_POSITION = 1;
    int MARKETS_TAB_POSITION = 2;
    int CARD_TAB_POSITION = 3;
    int PROFILE_TAB_POSITION = 4;

    @StateStrategyType(OneExecutionStateStrategy.class)
    void exit();


}
