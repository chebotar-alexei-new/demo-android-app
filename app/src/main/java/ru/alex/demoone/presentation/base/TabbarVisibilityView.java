package ru.alex.demoone.presentation.base;

/**
 * Created by alex on 01.11.17.
 */

public interface TabbarVisibilityView {
    void showTabbar(boolean b);
}
