package ru.alex.demoone.presentation.base.adapter;

import android.support.annotation.NonNull;

public class NoSuchRecyclerRowException extends RuntimeException {

    public NoSuchRecyclerRowException(@NonNull final RecyclerViewItem recyclerViewItem) {
        super("No cell for item = " + recyclerViewItem);
    }

    public NoSuchRecyclerRowException(final int viewType) {
        super("No cell for type = " + viewType);
    }
}
