package ru.alex.demoone.presentation.base;

public class Screens {

    public static final String LOGIN_SCREEN = "saveUser screen";

    public static final String TAB_MAIN_SCREEN = "tab main screen";
    public static final String TAB_CATALOG_SCREEN = "tab catalog screen";
    public static final String TAB_MARKETS_SCREEN = "tab markets screen";
    public static final String TAB_CARD_SCREEN = "tab card screen";
    public static final String TAB_PROFILE_SCREEN = "tab profile screen";

    public static final String MAIN_SCREEN = "main screen";

    public static final String CATALOG_SCREEN = "catalog screen";

    public static final String MARKETS_SCREEN = "markets screen";

    public static final String CARD_SCREEN = "card screen";

    public static final String PROFILE_SCREEN = "profile screen";


    public static final String URL_SCREEN = "url screen";
}
