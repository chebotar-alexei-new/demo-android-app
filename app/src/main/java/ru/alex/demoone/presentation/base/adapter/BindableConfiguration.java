package ru.alex.demoone.presentation.base.adapter;

import android.support.annotation.NonNull;

public class BindableConfiguration<TItem extends RecyclerViewItem> {

    @NonNull
    private final TItem item;
    private final int position;

    public BindableConfiguration(@NonNull final TItem item, final int position) {
        this.item = item;
        this.position = position;
    }

    @NonNull
    public TItem getItem() {
        return item;
    }

    public int getPosition() {
        return position;
    }
}
