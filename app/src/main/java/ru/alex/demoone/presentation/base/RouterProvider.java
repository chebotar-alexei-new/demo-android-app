package ru.alex.demoone.presentation.base;

import ru.terrakok.cicerone.Router;

public interface RouterProvider {
    Router getRouter();
}
