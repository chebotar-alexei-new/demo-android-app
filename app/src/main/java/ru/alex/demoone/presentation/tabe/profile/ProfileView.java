package ru.alex.demoone.presentation.tabe.profile;

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import ru.alex.demoone.presentation.base.MvpBaseView;

@StateStrategyType(OneExecutionStateStrategy.class)
public interface ProfileView extends MvpBaseView {

    void setInitData(String data);
}
