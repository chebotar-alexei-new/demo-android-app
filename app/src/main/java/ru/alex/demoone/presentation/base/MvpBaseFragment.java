package ru.alex.demoone.presentation.base;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;

import java.util.function.Supplier;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import ru.alex.demoone.R;
import ru.alex.demoone.data.network.RetrofitException;
import ru.alex.demoone.presentation.bottom.BottomNavigationActivity;

public abstract class MvpBaseFragment extends MvpAppCompatFragment implements MvpBaseView, BackButtonListener {

    protected View rootView;

    @Nullable
    @BindView(R.id.progressBar)
    View progressBar;
    @Nullable
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private Toast toast;
    protected CompositeDisposable compositeDisposable = new CompositeDisposable();
    protected Unbinder unbinder;
    private boolean shouldNotAnimate = false;

    protected void addSub(Disposable subscription) {
        compositeDisposable.add(subscription);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null && !compositeDisposable.isDisposed())
            compositeDisposable.dispose();
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return shouldNotAnimate
                ? AnimationUtils.loadAnimation(getActivity(), R.anim.none)
                : super.onCreateAnimation(transit, enter, nextAnim);
    }

    public void setShouldNotAnimate(boolean shouldNotAnimate) {
        this.shouldNotAnimate = shouldNotAnimate;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.shouldNotAnimate = false;
    }

    public MvpBaseFragment() {
    }

    protected abstract int setLayoutRes();

    /**
     * Вызываеться после бинда вьюшек
     */
    protected void onPostCreateView() {
        // nothing
    }

    /**
     * Для настройки тулбара
     */
    @CallSuper
    protected void configureToolbar(@NonNull final Toolbar toolbar) {
        if (showBackButton()) {
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(view -> onBackPressed());
        } else {
            toolbar.setNavigationIcon(null);
            toolbar.setOnMenuItemClickListener(null);
        }
    }

    /**
     * Переопределить чтобы проказать кнопку назад в тулбаре
     */
    protected boolean showBackButton() {
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(setLayoutRes(), container, false);
        unbinder = ButterKnife.bind(this, rootView);
        setupUI(rootView);
        onPostCreateView();
        if (toolbar != null) {
            configureToolbar(toolbar);
        }
        return rootView;
    }


    protected void setupUI(View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener((v, event) -> {
                hideSoftKeyboard(getActivity());
                return false;
            });
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    protected void hideSoftKeyboard(Activity activity) {
        if (activity == null) return;
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected void showToast(String message) {
        if (toast != null) toast.cancel();
        toast = Toast.makeText(getContext(), message, Toast.LENGTH_LONG);
        toast.show();
    }

    protected void showToast(int message) {
        if (toast != null) toast.cancel();
        toast = Toast.makeText(getContext(), message, Toast.LENGTH_LONG);
        toast.show();
    }

    protected void showDialog(Context context, String title, String message) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, null)
                .create()
                .show();
    }

    protected void showDialog(Context context, String title, String message, DialogInterface.OnClickListener lis) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, lis)
                .create()
                .show();
    }

    @Override
    public void showConnectionError() {
        showToast(getString(R.string.error_connection));
    }


    @Override
    public void showServerError(Throwable throwable) {
        throwable.printStackTrace();
        if (throwable instanceof RetrofitException && ((RetrofitException) throwable).errorMessageToUser != null)
            showToast(((RetrofitException) throwable).errorMessageToUser);
        else if (throwable instanceof RetrofitException && ((RetrofitException) throwable).getKind() != RetrofitException.Kind.NETWORK)
            showConnectionError();
        else
            showToast(getString(R.string.error_server_failed));
    }

    @Override
    public void showInternalError() {
        showToast(getString(R.string.error_internal));
    }

    @Override
    public void showErrorMessage(String s) {
        showToast(s);
    }

    @Override
    public void showErrorMessage(int s) {
        showToast(s);
    }

    @Override
    public void showLoading(boolean b) {
        if (progressBar != null)
            progressBar.setVisibility(b ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showTabbar(boolean b) {
        if (getActivity() != null && getActivity() instanceof BottomNavigationActivity) {
            ((BottomNavigationActivity) getActivity()).showTabbar(b);
        }
    }
}
