package ru.alex.demoone.presentation.tabb.catalog;

import com.arellomobile.mvp.InjectViewState;

import ru.terrakok.cicerone.Router;
import ru.alex.demoone.App;
import ru.alex.demoone.presentation.base.MvpBasePresenter;


@InjectViewState
public class CatalogPresenter extends MvpBasePresenter<CatalogView> {
    private Router router;

    CatalogPresenter(Router router) {
        App.INSTANCE.getAppComponent().inject(this);
        this.router = router;
    }

    @Override
    public void attachView(CatalogView view) {
        super.attachView(view);
        getViewState().setInitData("Catalog");
    }

    public void onBackPressed() {
        router.exit();
    }
}
