package ru.alex.demoone.presentation.bottom;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.android.SupportAppNavigator;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Forward;
import ru.alex.demoone.App;
import ru.alex.demoone.R;
import ru.alex.demoone.presentation.base.BackButtonListener;
import ru.alex.demoone.presentation.base.MvpBaseFragment;
import ru.alex.demoone.presentation.base.RouterProvider;
import ru.alex.demoone.presentation.base.Screens;
import ru.alex.demoone.presentation.taba.main.MainFragment;
import ru.alex.demoone.presentation.tabb.catalog.CatalogFragment;
import ru.alex.demoone.presentation.tabc.market.MarketFragment;
import ru.alex.demoone.presentation.tabd.card.CardFragment;
import ru.alex.demoone.presentation.tabe.login.LoginFragment;
import ru.alex.demoone.presentation.tabe.profile.ProfileFragment;

public class TabContainerFragment extends Fragment implements RouterProvider, BackButtonListener {

    private static final String EXTRA_NAME = "tcf_extra_name";

    private Navigator navigator;

    @Inject
    LocalCiceroneHolder ciceroneHolder;

    public static TabContainerFragment getNewInstance(String name) {
        TabContainerFragment fragment = new TabContainerFragment();
        Bundle arguments = new Bundle();
        arguments.putString(EXTRA_NAME, name);
        fragment.setArguments(arguments);
        return fragment;
    }

    public static TabContainerFragment getNewInstance(String name, Bundle bundle) {
        TabContainerFragment fragment = new TabContainerFragment();
        bundle.putString(EXTRA_NAME, name);
        fragment.setArguments(bundle);
        return fragment;
    }

    private String getContainerName() {
        return getArguments().getString(EXTRA_NAME);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        App.INSTANCE.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    private Cicerone<Router> getCicerone() {
        return ciceroneHolder.getCicerone(getContainerName());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tab_container, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getChildFragmentManager().findFragmentById(R.id.ftc_container) == null) {
            getCicerone().getRouter().replaceScreen(getContainerName(), getArguments());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getCicerone().getNavigatorHolder().setNavigator(getNavigator());
    }

    @Override
    public void onPause() {
        getCicerone().getNavigatorHolder().removeNavigator();
        super.onPause();
    }

    private Navigator getNavigator() {
        if (navigator == null) {
            navigator = new SupportAppNavigator(getActivity(), getChildFragmentManager(), R.id.ftc_container) {

                @Override
                protected Intent createActivityIntent(String screenKey, Object data) {
                    switch (screenKey) {
                        case Screens.URL_SCREEN:
                            return new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/"));
                        default:
                            return null;
                    }
                }

                @Override
                protected Fragment createFragment(String screenKey, Object data) {
                    switch (screenKey) {

                        case Screens.MAIN_SCREEN:
                            return MainFragment.getNewInstance((Bundle) data);
                        case Screens.MARKETS_SCREEN:
                            return MarketFragment.getNewInstance((Bundle) data);
                        case Screens.CATALOG_SCREEN:
                            return CatalogFragment.getNewInstance((Bundle) data);
                        case Screens.CARD_SCREEN:
                            return CardFragment.getNewInstance((Bundle) data);
                        case Screens.PROFILE_SCREEN:
                            return ProfileFragment.getNewInstance((Bundle) data);
                        case Screens.LOGIN_SCREEN:
                            return LoginFragment.getNewInstance((Bundle) data);

                        default:
                            return null;
                    }
                }


                @Override
                protected void setupFragmentTransactionAnimation(Command command,
                                                                 Fragment currentFragment,
                                                                 Fragment nextFragment,
                                                                 FragmentTransaction fragmentTransaction) {
                    if (command instanceof Forward) {
                        if (currentFragment instanceof CatalogFragment && nextFragment instanceof CardFragment) {
                            fragmentTransaction
                                    .setCustomAnimations(
                                            R.anim.slide_in_left,
                                            R.anim.slide_in_right,
                                            R.anim.slide_in_left_back,
                                            R.anim.slide_in_right_back);
                        }
                    }
                }

                @Override
                protected void exit() {
                    ((RouterProvider) getActivity()).getRouter().exit();
                }
            };
        }
        return navigator;
    }

    @Override
    public Router getRouter() {
        return getCicerone().getRouter();
    }

    @Override
    public boolean onBackPressed() {
        Fragment fragment = getChildFragmentManager().findFragmentById(R.id.ftc_container);
        if (fragment != null
                && fragment instanceof BackButtonListener
                && ((BackButtonListener) fragment).onBackPressed()) {
            return true;
        } else {
            ((RouterProvider) getActivity()).getRouter().exit();
            return true;
        }
    }

    public void setShouldNotAnimate(boolean b) {
        if (getChildFragmentManager().findFragmentById(R.id.ftc_container) != null)
            ((MvpBaseFragment) getChildFragmentManager().findFragmentById(R.id.ftc_container)).setShouldNotAnimate(b);
    }
}
