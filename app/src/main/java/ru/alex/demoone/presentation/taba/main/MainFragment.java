package ru.alex.demoone.presentation.taba.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import butterknife.BindView;
import ru.alex.demoone.R;
import ru.alex.demoone.presentation.base.BackButtonListener;
import ru.alex.demoone.presentation.base.MvpBaseFragment;
import ru.alex.demoone.presentation.base.RouterProvider;

public class MainFragment extends MvpBaseFragment implements MainView, BackButtonListener {

    private static final String TAG = MainFragment.class.getSimpleName();

    @BindView(R.id.anyText)
    TextView anyTextView;
    @InjectPresenter
    MainPresenter presenter;

    @ProvidePresenter
    MainPresenter provideForwardPresenter() {
        return new MainPresenter(((RouterProvider) getParentFragment()).getRouter());
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_main;
    }

    public static MainFragment getNewInstance(Bundle data) {
        MainFragment fragment = new MainFragment();
        fragment.setArguments(data);
        return fragment;
    }

    public static Bundle getBundle() {
        Bundle arguments = new Bundle();
        return arguments;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews();
    }

    private void initViews() {

    }

    @Override
    public boolean onBackPressed() {
        presenter.onBackPressed();
        return true;
    }

    @Override
    public void setInitData(String data) {
        anyTextView.setText(data);
    }


}
