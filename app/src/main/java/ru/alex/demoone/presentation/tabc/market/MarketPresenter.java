package ru.alex.demoone.presentation.tabc.market;

import com.arellomobile.mvp.InjectViewState;

import ru.terrakok.cicerone.Router;
import ru.alex.demoone.App;
import ru.alex.demoone.presentation.base.MvpBasePresenter;
import ru.alex.demoone.presentation.tabd.card.CardView;


@InjectViewState
public class MarketPresenter extends MvpBasePresenter<MarketView> {
    private Router router;

    MarketPresenter(Router router) {
        App.INSTANCE.getAppComponent().inject(this);
        this.router = router;
    }

    @Override
    public void attachView(MarketView view) {
        super.attachView(view);
        getViewState().setInitData("Market");
    }

    public void onBackPressed() {
        router.exit();
    }
}
