package ru.alex.demoone.presentation.base;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface MvpBaseView extends SimpleErrorView, LoadingView, MvpView, TabbarVisibilityView {

}
