package ru.alex.demoone.presentation.bottom;

import android.os.Handler;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;
import ru.alex.demoone.App;
import ru.alex.demoone.R;
import ru.alex.demoone.domain.interactors.UserInteractor;
import ru.alex.demoone.presentation.base.MvpBasePresenter;
import ru.alex.demoone.presentation.base.Screens;

@InjectViewState
public class BottomNavigationPresenter extends MvpBasePresenter<IBottomNavigationView> {

    private Router router;

    @Inject
    UserInteractor userInteractor;

    private boolean doubleBackToExitPressedOnce;

    BottomNavigationPresenter(Router router) {
        App.INSTANCE.getAppComponent().inject(this);
        this.router = router;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
    }

    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            getViewState().exit();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        getViewState().showErrorMessage(R.string.press_back_again_to_exit);
        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2_000);
    }

    void onTabMainClick() {
        router.replaceScreen(Screens.TAB_MAIN_SCREEN);
    }

    void onTabCatalogClick() {
        router.replaceScreen(Screens.TAB_CATALOG_SCREEN);
    }

    void onTabMarketsClick() {
        router.replaceScreen(Screens.TAB_MARKETS_SCREEN);
    }

    void onTabCardClick() {
        router.replaceScreen(Screens.TAB_CARD_SCREEN);
    }

    void onTabProfileClick() {
        router.replaceScreen(Screens.TAB_PROFILE_SCREEN);
    }

    boolean isUserLogined() {
        return userInteractor.isUserLogined();
    }
}
