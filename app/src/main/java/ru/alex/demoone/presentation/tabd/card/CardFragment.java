package ru.alex.demoone.presentation.tabd.card;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import butterknife.BindView;
import ru.alex.demoone.R;
import ru.alex.demoone.presentation.base.BackButtonListener;
import ru.alex.demoone.presentation.base.MvpBaseFragment;
import ru.alex.demoone.presentation.base.RouterProvider;

public class CardFragment extends MvpBaseFragment implements CardView, BackButtonListener {

    private static final String TAG = CardFragment.class.getSimpleName();

    @BindView(R.id.anyText)
    TextView anyTextView;
    @InjectPresenter
    CardPresenter presenter;

    @ProvidePresenter
    CardPresenter provideForwardPresenter() {
        return new CardPresenter(((RouterProvider) getParentFragment()).getRouter());
    }

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_card;
    }

    public static CardFragment getNewInstance(Bundle data) {
        CardFragment fragment = new CardFragment();
        fragment.setArguments(data);
        return fragment;
    }

    public static Bundle getBundle() {
        Bundle arguments = new Bundle();
        return arguments;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews();
    }

    private void initViews() {

    }

    @Override
    public boolean onBackPressed() {
        presenter.onBackPressed();
        return true;
    }

    @Override
    public void setInitData(String data) {
        anyTextView.setText(data);
    }
}
