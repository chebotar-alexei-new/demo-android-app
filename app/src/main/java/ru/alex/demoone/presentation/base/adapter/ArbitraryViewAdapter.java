package ru.alex.demoone.presentation.base.adapter;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

public abstract class ArbitraryViewAdapter extends ListAdapter<RecyclerViewItem, RecyclerView.ViewHolder> {

    @NonNull
    protected ArbitraryViewSelector arbitraryCellSelector = new ArbitraryViewSelector();

    protected ArbitraryViewAdapter(@NonNull final DiffUtil.ItemCallback<RecyclerViewItem> diffCallback) {
        super(diffCallback);
    }

    @Override
    public final int getItemViewType(final int position) {
        return arbitraryCellSelector.getCell(getItem(position)).type();
    }

    @NonNull
    @Override
    public final RecyclerView.ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        return arbitraryCellSelector.getCell(viewType).holder(parent);
    }

    @Override
    public final void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        final RecyclerViewItem item = getItem(position);
        arbitraryCellSelector.getCell(item).bind(holder, getBindableConfiguration(item, position));
    }

    @NonNull
    protected BindableConfiguration<RecyclerViewItem> getBindableConfiguration(@NonNull final RecyclerViewItem item, final int pos) {
        return new BindableConfiguration<>(item, pos);
    }
}