package ru.alex.demoone.presentation.tabe.login;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import ru.alex.demoone.presentation.base.MvpBaseView;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface LoginView extends MvpBaseView {

}
