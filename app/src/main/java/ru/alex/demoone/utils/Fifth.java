package ru.alex.demoone.utils;

/**
 * Аналог Pair.
 */
public class Fifth<A, B, C, D, E> {

    public A first;
    public B second;
    public C third;
    public D four;
    public E fifth;

    public Fifth(A first, B second, C third, D four, E fifth) {
        this.first = first;
        this.second = second;
        this.third = third;
        this.four = four;
        this.fifth = fifth;
    }
}
