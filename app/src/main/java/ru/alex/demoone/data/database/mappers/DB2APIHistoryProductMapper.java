package ru.alex.demoone.data.database.mappers;


import ru.alex.demoone.data.database.base.Mapping;
import ru.alex.demoone.data.database.models.DBHistoryProduct;
import ru.alex.demoone.data.network.models.Product;

public class DB2APIHistoryProductMapper extends Mapping<DBHistoryProduct, Product> {

    @Override
    public Product map(DBHistoryProduct dbProduct) {
        return new Product(dbProduct.getImage(),
                dbProduct.getTitle(),
                dbProduct.getId(),
                dbProduct.getPrice(),
                dbProduct.getPriceOld(),
                dbProduct.getSalePercent());
    }
}
