package ru.alex.demoone.data.database.base;

/**
 * Базовый класс для маппинга моделей.
 */
public abstract class Mapping<From, To> {

    public abstract To map(From from);
}
