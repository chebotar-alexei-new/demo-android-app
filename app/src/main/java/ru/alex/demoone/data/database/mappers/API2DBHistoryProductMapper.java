package ru.alex.demoone.data.database.mappers;


import ru.alex.demoone.data.database.base.Mapping;
import ru.alex.demoone.data.database.models.DBHistoryProduct;
import ru.alex.demoone.data.network.models.Product;

public class API2DBHistoryProductMapper extends Mapping<Product, DBHistoryProduct> {

    @Override
    public DBHistoryProduct map(Product product) {
        DBHistoryProduct dbProduct = new DBHistoryProduct();
        dbProduct.setId(product.id);
        dbProduct.setPrice(product.price);
        dbProduct.setPriceOld(product.priceOld);
        dbProduct.setSalePercent(product.salePercent);
        dbProduct.setTitle(product.title);
        return dbProduct;
    }
}
