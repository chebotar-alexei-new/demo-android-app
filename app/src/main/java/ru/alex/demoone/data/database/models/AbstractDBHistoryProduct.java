package ru.alex.demoone.data.database.models;

import io.requery.Entity;
import io.requery.Index;
import io.requery.Key;

/**
 * Класс для создания таблицы в БД.
 */
@Entity
public class AbstractDBHistoryProduct {

    @Key
    String id;

    String image;

    @Index("title_index")
    String title;
    
    double price;
    double priceOld;
    String salePercent;

}
