package ru.alex.demoone.data.database.mappers;


import java.util.ArrayList;
import java.util.List;

import ru.alex.demoone.data.database.base.Mapping;
import ru.alex.demoone.data.database.models.DBHistoryProduct;
import ru.alex.demoone.data.network.models.Product;


public class API2DBHistoryProductListMapper extends Mapping<List<Product>, List<DBHistoryProduct>> {

    private final API2DBHistoryProductMapper api2DBHistoryProductMapper = new API2DBHistoryProductMapper();

    @Override
    public List<DBHistoryProduct> map(List<Product> transports) {
        ArrayList<DBHistoryProduct> dbMessages = new ArrayList<>(transports.size());
        for (Product transport : transports) {
            dbMessages.add(api2DBHistoryProductMapper.map(transport));
        }
        return dbMessages;
    }
}
