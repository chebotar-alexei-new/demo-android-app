package ru.alex.demoone.data.database.base;

import java.util.concurrent.Executors;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;


/**
 * Scheduler для операций с БД.
 */
public class DatabaseScheduler {

    private static final Scheduler scheduler = Schedulers.from(Executors.newSingleThreadExecutor());

    public static Scheduler get() {
        return scheduler;
    }
}
