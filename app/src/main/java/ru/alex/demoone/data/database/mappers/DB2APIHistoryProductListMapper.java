package ru.alex.demoone.data.database.mappers;


import java.util.ArrayList;
import java.util.List;

import ru.alex.demoone.data.database.base.Mapping;
import ru.alex.demoone.data.database.models.DBHistoryProduct;
import ru.alex.demoone.data.network.models.Product;


public class DB2APIHistoryProductListMapper extends Mapping<List<DBHistoryProduct>, List<Product>> {

    private final DB2APIHistoryProductMapper db2APIProductMapper = new DB2APIHistoryProductMapper();

    @Override
    public List<Product> map(List<DBHistoryProduct> dbProducts) {
        ArrayList<Product> transports = new ArrayList<>(dbProducts.size());
        for (DBHistoryProduct dbProduct : dbProducts) {
            transports.add(db2APIProductMapper.map(dbProduct));
        }
        return transports;
    }
}