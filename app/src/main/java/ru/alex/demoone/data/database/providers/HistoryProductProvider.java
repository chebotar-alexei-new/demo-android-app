package ru.alex.demoone.data.database.providers;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;
import ru.alex.demoone.data.database.base.AbstractProvider;
import ru.alex.demoone.data.database.mappers.API2DBHistoryProductMapper;
import ru.alex.demoone.data.database.mappers.DB2APIHistoryProductListMapper;
import ru.alex.demoone.data.database.mappers.DB2APIHistoryProductMapper;
import ru.alex.demoone.data.database.models.DBHistoryProduct;
import ru.alex.demoone.data.network.models.Product;

/**
 * Провайдер доступа к записям таблицы.
 */
@Singleton
public class HistoryProductProvider extends AbstractProvider {

    @Inject
    public HistoryProductProvider(ReactiveEntityStore<Persistable> entityStore) {
        super(entityStore);
    }

    /**
     * Сохранить данные о пользователе.
     */
    public Flowable<Product> putProduct(@NonNull Product product) {
        DBHistoryProduct dbHistoryProduct = new API2DBHistoryProductMapper().map(product);
        return entityStore.upsert(dbHistoryProduct)
                .subscribeOn(Schedulers.io())
                .map(dbHistoryProducts -> new DB2APIHistoryProductMapper().map(dbHistoryProducts))
                .toFlowable();
    }

    /**
     * Получить продукт по идентификатору.
     */
    public Observable<DBHistoryProduct> getProduct(String id) {
        return entityStore
                .select(DBHistoryProduct.class)
                .where(DBHistoryProduct.ID.eq(id))
                .limit(1)
                .get()
                .observable()
                .subscribeOn(Schedulers.io());
    }

    /**
     * Получить продукты.
     */
    public Flowable<List<Product>> getProducts() {
        return entityStore
                .select(DBHistoryProduct.class)
                .get()
                .observable()
                .toList()
                .toFlowable()
                .subscribeOn(Schedulers.io())
                .map(dbHistoryProducts -> new DB2APIHistoryProductListMapper().map(dbHistoryProducts));
    }

    /**
     * Подписаться на список всех продуктов при изменениях в таблице.
     */
    public Observable<List<DBHistoryProduct>> getProductsChanges() {
        return entityStore
                .select(DBHistoryProduct.class)
                .get()
                .observableResult()
                .flatMap(userChanges -> userChanges.observable().toList().toObservable());
    }

    /**
     * Очистить таблицу.
     */
    public void clear() {
        Integer val = entityStore
                .delete(DBHistoryProduct.class)
                .get()
                .single()
                .subscribeOn(Schedulers.io())
                .blockingGet();
    }

    /**
     * Получить продукты по вхождению поисковой строки.
     */
    public Flowable<List<Product>> getProducts(String query) {
        return entityStore
                .select(DBHistoryProduct.class)
                .where(DBHistoryProduct.TITLE.lower().like("%"+query.toLowerCase()+"%"))
                .get()
                .observable()
                .toList()
                .toFlowable()
                .subscribeOn(Schedulers.io())
                .map(dbHistoryProducts -> new DB2APIHistoryProductListMapper().map(dbHistoryProducts));

    }
}
