package ru.alex.demoone.data.network.interceptors;

import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Response;
import okhttp3.ResponseBody;
import ru.alex.demoone.BuildConfig;
import ru.alex.demoone.data.network.models.Result;
import ru.alex.demoone.data.preference.Preferences;

/**
 * Преобразует хедеры из ответа в поле резалта.
 */
public class HeadersToResultInterceptor implements Interceptor {

    private final Preferences preferences;

    public HeadersToResultInterceptor(Preferences preferences) {
        this.preferences = preferences;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());
        if (chain.request().url().url().getHost().contains(BuildConfig.HOST)) {
            Result result = new Gson().fromJson(response.body().string(), Result.class);
            result.headers = response.headers().toMultimap();
            String userId = null;
            if (result.headers.containsKey("f-user-id") && !((ArrayList<String>) result.headers.get("f-user-id")).isEmpty())
                userId = ((ArrayList<String>) result.headers.get("f-user-id")).get(0);
            if (userId != null) {
                preferences.setFUserId(userId);
                if (BuildConfig.DEBUG)
                    Log.d("f-user-id", "saved from response " + preferences.getFUserId());
            }
            String responseString = new Gson().toJson(result);
            return new Response.Builder()
                    .code(response.code())
                    .message(responseString)
                    .request(chain.request())
                    .protocol(Protocol.HTTP_1_0)
                    .body(ResponseBody.create(MediaType.parse("application/json"), responseString))
                    .addHeader("content-type", "application/json")
                    .build();
        } else {
            return response;
        }
    }
}