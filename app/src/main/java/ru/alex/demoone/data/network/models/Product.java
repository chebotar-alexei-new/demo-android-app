package ru.alex.demoone.data.network.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Product implements Parcelable {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("sku")
    @Expose
    public String sku;
    @SerializedName("list_image")
    @Expose
    public String listImage;
    @SerializedName("name")
    @Expose
    public String title;
    @SerializedName("rating")
    @Expose
    public float rating;
    @SerializedName("price")
    @Expose
    public double price;
    @SerializedName("old_price")
    @Expose
    public double priceOld;
    @SerializedName("sale_percent")
    @Expose
    public String salePercent;
    @SerializedName("currency")
    @Expose
    public String currency;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("composition")
    @Expose
    public String composition;
    @SerializedName("related")
    @Expose
    public List<Product> related;
    @SerializedName("similar")
    @Expose
    public List<Product> similar;
    @SerializedName("images_for_catalog_list")
    @Expose
    public List<String> listImages;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("video_url")
    @Expose
    public String videoUrl;
    @SerializedName("preview_video_url")
    @Expose
    public String previewVideoUrl;
    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("only_site")
    @Expose
    public boolean onlySite;
    @SerializedName("only_store")
    @Expose
    public boolean onlyStore;


    public Product() {
    }


    public Product(String image, String title, String id, double price, double priceOld, String sale) {
        this.title = title;
        this.id = id;
        this.price = price;
        this.priceOld = priceOld;
        this.salePercent = sale;
    }


    protected Product(Parcel in) {
        id = in.readString();
        sku = in.readString();
        listImage = in.readString();
        title = in.readString();
        rating = in.readFloat();
        price = in.readDouble();
        priceOld = in.readDouble();
        salePercent = in.readString();
        currency = in.readString();
        description = in.readString();
        composition = in.readString();
        related = in.createTypedArrayList(Product.CREATOR);
        similar = in.createTypedArrayList(Product.CREATOR);
        listImages = in.createStringArrayList();
        url = in.readString();
        videoUrl = in.readString();
        previewVideoUrl = in.readString();
        code = in.readString();
        onlySite = in.readByte() != 0;
        onlyStore = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(sku);
        dest.writeString(listImage);
        dest.writeString(title);
        dest.writeFloat(rating);
        dest.writeDouble(price);
        dest.writeDouble(priceOld);
        dest.writeString(salePercent);
        dest.writeString(currency);
        dest.writeString(description);
        dest.writeString(composition);
        dest.writeTypedList(related);
        dest.writeTypedList(similar);
        dest.writeStringList(listImages);
        dest.writeString(url);
        dest.writeString(videoUrl);
        dest.writeString(previewVideoUrl);
        dest.writeString(code);
        dest.writeByte((byte) (onlySite ? 1 : 0));
        dest.writeByte((byte) (onlyStore ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
}
