package ru.alex.demoone.data.network.interceptors;

import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import ru.alex.demoone.BuildConfig;
import ru.alex.demoone.data.preference.Preferences;

/**
 * Навешивает в хедер accessToken.
 */
public class AuthenticationInterceptor implements Interceptor {
    private Preferences preferences;

    public AuthenticationInterceptor(Preferences preferences) {
        this.preferences = preferences;

    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        if (chain.request().url().url().getHost().contains(BuildConfig.HOST)) {
            Request.Builder builder = chain.request().newBuilder()
                    .header("Accept", "application/json");
            if (preferences.getUser() != null && !TextUtils.isEmpty(preferences.getUser().accessToken)) {
                builder.header("access-token", preferences.getUser().accessToken);
                if (BuildConfig.DEBUG) Log.d("access-token", preferences.getUser().accessToken);
            }
            if (!preferences.getFUserId().isEmpty()) {
                builder.header("f-user-id", preferences.getFUserId());
                if (BuildConfig.DEBUG)
                    Log.d("f-user-id", "added to request " + preferences.getFUserId());
            }
            Request compressedRequest = builder.build();
            return chain.proceed(compressedRequest);
        } else {
            return chain.proceed(chain.request());
        }
    }

}
