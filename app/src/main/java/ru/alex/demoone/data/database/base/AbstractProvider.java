package ru.alex.demoone.data.database.base;

import android.support.annotation.NonNull;

import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;

/**
 * Базовый провайдер операций с БД.
 */
public abstract class AbstractProvider {

    protected final ReactiveEntityStore<Persistable> entityStore;

    public AbstractProvider(@NonNull ReactiveEntityStore<Persistable> entityStore) {
        this.entityStore = entityStore;
    }
}
