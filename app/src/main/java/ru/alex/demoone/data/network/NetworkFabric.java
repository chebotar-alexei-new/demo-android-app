package ru.alex.demoone.data.network;

import android.app.Application;
import android.util.Log;

import java.util.concurrent.Semaphore;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.FlowableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import ru.alex.demoone.data.network.models.Result;
import ru.alex.demoone.data.preference.Preferences;
import ru.alex.demoone.presentation.bottom.BottomNavigationActivity;

/**
 * Класс предоставляющий FlowableTransformer для рефреша токена.
 * Восстанавливает запрос если токен сетится через AuthenticationInterceptor.
 */
@Singleton
public class NetworkFabric {

    private final ServerAPI serverAPI;
    private final Preferences preferences;
    private final Application application;
    public final Semaphore semaphore = new Semaphore(1);

    @Inject
    public NetworkFabric(ServerAPI serverAPI, Preferences preferences, Application application) {
        this.serverAPI = serverAPI;
        this.preferences = preferences;
        this.application = application;
    }


    public <T> Function<Throwable, ? extends Flowable<Result<T>>> refreshToken
            (final Flowable<Result<T>> toBeResumed, Semaphore sem) {
        return (Function<Throwable, Flowable<Result<T>>>) throwable -> {
            if (throwable instanceof RetrofitException && ((RetrofitException) throwable).code == 401) {
                try {
                    int i = 0;
                    String preToken = null;
                    if (preferences.getUser() != null)
                        preToken = preferences.getUser().accessToken;
                    Log.d("refreshToken", stringMiniId(toBeResumed) + " Поток заблокирован");
                    sem.acquire();
                    Log.d("refreshToken", stringMiniId(toBeResumed) + " Поток проверяет токен");
                    if (preferences.getUser() != null && preToken != null && preToken.equals(preferences.getUser().accessToken)) {
                        Log.d("refreshToken", stringMiniId(toBeResumed) + " Токен не обновлен, обновить");
                        i++;
                        Log.d("refreshToken", "i = " + i);
                        return serverAPI.refreshToken(preferences.getUser().refreshToken)
                                .onErrorResumeNext(t -> {
                                    preferences.setUser(null);
                                    sem.release();
                                    restart();
                                })
                                .switchMap(userResult -> {
                                    preferences.setUser(userResult.data);
                                    sem.release();
                                    Log.d("refreshToken", stringMiniId(toBeResumed) +
                                            " Поток освобожден после обновления токена");
                                    return toBeResumed;
                                });
                    } else {
                        Log.d("refreshToken", stringMiniId(toBeResumed) +
                                " Поток освобожден, Токен релевантен, перезапустить запрос");
                        sem.release();
                        return toBeResumed;
                    }
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                    return Flowable.error(throwable);
                }
            } else {
                return Flowable.error(throwable);
            }
        };
    }

    private String stringMiniId(Object toBeResumed) {
        return toBeResumed.toString().substring(toBeResumed.toString().length() - 2);
    }


    public <T> FlowableTransformer<Result<T>, Result<T>> applyRefreshAndCheck(final Flowable<Result<T>> toBeResumed) {
        return observable -> observable
                .onErrorResumeNext(refreshToken(toBeResumed, semaphore))
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach();
    }

    private void restart() {
        BottomNavigationActivity.restart(application);
    }

    public <T> FlowableTransformer<T, T> dbTransformer() {
        return observable -> observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach();
    }

}
