package ru.alex.demoone.repositories;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import ru.alex.demoone.data.network.NetworkFabric;
import ru.alex.demoone.data.network.ServerAPI;
import ru.alex.demoone.data.network.models.Result;
import ru.alex.demoone.data.network.models.User;

@Singleton
public class LoginRepository extends BaseRepository {

    private final ServerAPI serverAPI;
    private final NetworkFabric networkFabric;

    @Inject
    public LoginRepository(ServerAPI serverAPI, NetworkFabric networkFabric) {
        this.serverAPI = serverAPI;
        this.networkFabric = networkFabric;
    }

    public Flowable<Result<User>> auth(String email, String pass) {
        return serverAPI.auth(email, pass)
                .compose(networkFabric.applyRefreshAndCheck(serverAPI.auth(email, pass)));
    }


    public Flowable<Result<User>> getUser() {
        return serverAPI.getUser()
                .compose(networkFabric.applyRefreshAndCheck(serverAPI.getUser()));
    }
}
